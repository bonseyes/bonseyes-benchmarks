## Contributing
Thank you for your interest in contributing to this Bonseyes dataset project! We welcome all contributions. By participating in this project, you accept and agree to the following terms and conditions for Your present and future contributions submitted to Bonseyes Community Association. Except for the license granted herein to Bonseyes Community Association. and recipients of software distributed by Bonseyes Community Association, you reserve all right, title, and interest in and to your contributions. All contributions are subject to the following DCO + License terms.

This notice should stay as the first item in the CONTRIBUTING.md file.

## Developer Certificate of Origin Version 1.1
Copyright (C) 2004, 2006 The Linux Foundation and its contributors, 1 Letterman Drive, Suite D4700, San Francisco, CA 94129, USA

Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed.

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I have the right to submit it under the open source license indicated in the file; or

(b) The contribution is based upon previous work that, to the best of my knowledge, is covered under an appropriate open source license and I have the right under that license to submit that work with modifications, whether created in whole or in part by me, under the same open source license (unless I am permitted to submit under a different license), as indicated in the file; or

(c) The contribution was provided directly to me by some other person who certified (a), (b) or (c) and I have not modified it.

(d) I understand and agree that this project and the contribution are public and that a record of the contribution (including all personal information I submit with it, including my sign-off) is maintained indefinitely and may be redistributed consistent with this project or the open source license(s) involved.

## License
All data content that resides under the results/ and platforms/ directory of this repository is licensed under Creative Commons: CC BY-SA 4.0.

